package monkeybros.connectjam.core.client;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.gwt.GwtApplication;
import com.badlogic.gdx.backends.gwt.GwtApplicationConfiguration;
import monkeybros.connectjam.core.GameSettings;
import monkeybros.connectjam.core.Jam;
import monkeybros.connectjam.core.TextToSpeech;

public class GwtLauncher extends GwtApplication {
	@Override
	public GwtApplicationConfiguration getConfig () {
		GwtApplicationConfiguration cfg = new GwtApplicationConfiguration(480, 320);
		return cfg;
	}

	@Override
	public ApplicationListener getApplicationListener () {
		return new Jam(new GameSettings(), new TextToSpeech() {
      @Override
      public void talk(String text) {
      }
    });
	}
}