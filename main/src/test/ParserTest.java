package test;

import monkeybros.connectjam.core.StateGraph;
import monkeybros.connectjam.core.parser.GraphParser;
import org.junit.Assert;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class ParserTest {

  @Test
  public void testParser() throws FileNotFoundException {
    GraphParser parser = new GraphParser();
    parser.setInput(new FileInputStream("test1.graphml"));
    StateGraph graph = parser.parse();
    Assert.assertEquals("d6", graph.getStateAttributes().get("description"));
    Assert.assertEquals("Beer or Wine?", graph.getStates().get("n0").getStateProperty("description"));
    Assert.assertEquals("n0", graph.getStartNode().getId());
    System.out.println(graph);
  }

}
