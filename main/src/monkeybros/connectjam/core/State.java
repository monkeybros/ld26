package monkeybros.connectjam.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class State extends GraphElem {

  public List<Transition> outgoing = new ArrayList<Transition>();
  public List<Transition> incoming = new ArrayList<Transition>();

  public State(StateGraph g, String id) {
    this.id = id;
    this.graph = g;
  }

  public <T> T getStateProperty(String key) {
    return (T) properties.get(graph.getStateAttributes().get(key));
  }

  @Override
  public String toString() {
    return "Node{" +
      "id='" + id + '\'' +
      ", properties=" + properties +
      ", outgoing=" + outgoing +
      '}';
  }
}
