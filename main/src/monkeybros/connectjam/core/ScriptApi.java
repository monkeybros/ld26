package monkeybros.connectjam.core;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;
import org.mozilla.javascript.ScriptableObject;

public class ScriptApi {

  public final Jam game;

  public ScriptApi(Jam game) {
    this.game = game;
  }

  public Music playMusic(String path) {
    Music music = Gdx.audio.newMusic(Gdx.files.internal(path));
    music.play();
    return music;
  }

  public Sound playSound(String path) {
    Sound sound = Gdx.audio.newSound(Gdx.files.internal(path));
    sound.play();
    return sound;
  }

  public void log(String message) {
    Gdx.app.log("script", message);
  }

  public static Object runScript(Jam game, String script) {

    // Create an execution environment.
    Context cx = Context.enter();

    // Turn compilation off.
    cx.setOptimizationLevel(-1);

    try
    {
      // Initialize a variable scope with bindings for
      // standard objects (Object, Function, etc.)
      Scriptable scope = cx.initStandardObjects();

      // Set a global variable that holds the activity instance.
      ScriptableObject.putProperty(
        scope, "_", Context.javaToJS(new ScriptApi(game), scope));

      // Evaluate the script.
      return cx.evaluateString(scope, script, "script:", 1, null);
    }
    finally
    {
      Context.exit();
    }

  }
}
