package monkeybros.connectjam.core;

public interface TextToSpeech {
  public void talk(String text);
}
