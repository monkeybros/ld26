package monkeybros.connectjam.core;

public class Transition extends GraphElem {

  String source;
  String target;

  public Transition(StateGraph graph, String id, String source, String target) {
    this.id = id;
    this.source = source;
    this.target = target;
    this.graph = graph;
  }

  public String getSource() {
    return source;
  }

  public void setSource(String source) {
    this.source = source;
  }

  public String getTarget() {
    return target;
  }

  public void setTarget(String target) {
    this.target = target;
  }

  public <T> T getTransitionProperty(String key) {
    return (T) properties.get(graph.getTransitionAttributes().get(key));
  }

  @Override
  public String toString() {
    return "Edge{" +
      "id='" + id + '\'' +
      ", source='" + source + '\'' +
      ", target='" + target + '\'' +
      ", label='" + label + '\'' +
      ", properties='" + properties + '\'' +
      '}';
  }
}
