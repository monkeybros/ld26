package monkeybros.connectjam.core;

import java.util.HashMap;
import java.util.Map;

public class GraphElem {

  protected String id;

  public String getId() {
    return id;
  }

  protected String label;

  protected StateGraph graph;

  protected Map<String, Object> properties = new HashMap<String, Object>();

  public Map<String, Object> getProperties() {
    return properties;
  }

  public void setProperty(String id, Object value) {
    properties.put(id, value);
  }

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }
}
