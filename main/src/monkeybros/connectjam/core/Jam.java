package monkeybros.connectjam.core;

import aurelienribon.tweenengine.TweenManager;
import aurelienribon.tweenengine.Tween;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.*;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import monkeybros.connectjam.core.parser.GraphParser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class Jam extends Game {

  private final GameSettings settings;
  TextToSpeech tts;
  Skin skin;
  Stage stage;
  SpriteBatch batch;
  private Table layout;
  TweenManager tweenmanager;
  public Map<String, Object> variables = new HashMap<String, Object>();

  public Jam(GameSettings settings, TextToSpeech tts) {
    this.settings = settings;
    this.tts = tts;
  }

  public float rf = 0.0f;
  public float gf = 0.0f;
  public float bf = 0.0f;

  private int scrnWidth;
  private int scrnHeight;

  private TiledMapHandler tiledMapHandler;

  @Override
  public void create() {
    batch = new SpriteBatch();
    stage = new Stage();
    Gdx.input.setInputProcessor(stage);
    skin = new Skin();

    layout = new Table();
    layout.setFillParent(true);
    stage.addActor(layout);

    // A skin can be loaded via JSON or defined programmatically, either is fine. Using a skin is optional but strongly
    // recommended solely for the convenience of getting a texture, region, etc as a drawable, tinted drawable, etc.
    initSkin(skin);
    initInput(stage);
    initMenu(stage);
  }

  private void initMenu(final Stage stage) {
    layout.clear();

    Window window = new Window("", skin);
    layout.add(window).center().size(200, 200);

    Properties props = new Properties();
    java.util.List<String> games = new ArrayList<String>();

    try {
      props.load(Gdx.files.internal("stories").read());
    } catch (Exception e) {
      e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
    }

    for (Map.Entry entry: props.entrySet()) {
      String key = (String) entry.getKey();
      games.add(key.substring(0, key.indexOf(".")));
    }

    SelectBox dropdown = new SelectBox(games.toArray(), skin);
    window.add(new Label("Game:", skin, "black"));
    window.add(dropdown).center();
    window.row();

    TextButton startButton = new TextButton("Start", skin);
    window.add(startButton).fillX().colspan(2).spaceTop(20);

    startButton.addListener(new ChangeListener(){
      @Override
      public void changed(ChangeEvent event, Actor actor) {
        initLayout(stage);
        final StateGraph graph = new GraphParser("leslie.graphml").parse();
        enterState(graph.getStartNode(), graph, layout);
      }
    });
  }

  private void initInput(Stage stage) {
    stage.addListener(new InputListener() {
      @Override
      public boolean keyUp(InputEvent event, int keycode) {
        if (keycode == Input.Keys.ESCAPE) {
          Gdx.app.exit();
          return true;
        }
        return false;
      }
    });
  }

  private void
  initLayout(Stage stage) {
    layout.clear();

    scrnWidth = Gdx.graphics.getWidth();
    scrnHeight = Gdx.graphics.getHeight();

    tiledMapHandler = new TiledMapHandler();

    tiledMapHandler.setPackageDirectory("tilesets");

    tiledMapHandler.loadMap("maps/SpaceHouse.tmx");

    tiledMapHandler.initCamera(scrnWidth, scrnHeight);

    tiledMapHandler.getCamera().position.x = scrnWidth/2.0f;
    tiledMapHandler.getCamera().position.y = scrnHeight/2.0f;


    tweenmanager = new TweenManager();
    Tween.registerAccessor(TiledMapObject.class, new TiledMapObjectAccessor());
    tweenmanager.add(Tween.to(tiledMapHandler.getObjectList().get(0), TiledMapObjectAccessor.POSITION_X, 15).target(400));

  }

  private void initSkin(Skin skin) {
    // Generate a 1x1 white texture and store it in the skin named "white".
    Pixmap pixmap = new Pixmap(1, 1, Pixmap.Format.RGBA8888);
    pixmap.setColor(Color.WHITE);
    pixmap.fill();
    skin.add("white", new Texture(pixmap));
    String font = "arial-24";
    // Store the default libgdx font under the name "default".
    BitmapFont bitmapFont = new BitmapFont (Gdx.files.internal("ui/"+font+".fnt")
      ,Gdx.files.internal("ui/"+font+".png"), false, true);
    skin.add("default", bitmapFont);

    // Configure a TextButtonStyle and name it "default". Skin resources are stored by type, so this doesn't overwrite the font.
    TextButton.TextButtonStyle textButtonStyle = new TextButton.TextButtonStyle();
    textButtonStyle.up = skin.newDrawable("white", new Color(1, 0.5f, 0, 1));
    textButtonStyle.down = skin.newDrawable("white", new Color(1, 0.5f, 0, 1));
    textButtonStyle.over = skin.newDrawable("white", Color.LIGHT_GRAY);
    textButtonStyle.font = skin.getFont("default");
    skin.add("default", textButtonStyle);

    Label.LabelStyle labelStyle = new Label.LabelStyle(bitmapFont, Color.WHITE);
    skin.add("default", labelStyle);

    Label.LabelStyle blackLabelStyle = new Label.LabelStyle(bitmapFont, Color.BLACK);
    skin.add("black", blackLabelStyle);

    Window.WindowStyle windowStyle = new Window.WindowStyle(bitmapFont, Color.WHITE, skin.newDrawable("white", Color.CYAN));
    skin.add("default", windowStyle);

    SelectBox.SelectBoxStyle selectBoxStyle = new SelectBox.SelectBoxStyle(bitmapFont, Color.WHITE,
      skin.newDrawable("white", Color.RED),skin.newDrawable("white", Color.YELLOW) , skin.newDrawable("white", Color.BLUE));
    skin.add("default", selectBoxStyle);
  }

  private void enterState(State node, final StateGraph graph, final Table layout) {
    layout.clear();
    layout.row();

    String stateText = node.getStateProperty("description");

    if (settings.isTtsEnabled()) {
      tts.talk(stateText);
    }

    executeScript((String) node.getStateProperty("onEnter"));

    final Label myLabel = new Label(stateText, skin);
    myLabel.setWrap(true);
    myLabel.setFontScale(1.0f);
    myLabel.getStyle().background = skin.newDrawable("white",new Color(0.2f,0.2f,0.2f,0.75f));
    layout.add(myLabel).size(Gdx.graphics.getWidth() - 30, Gdx.graphics.getHeight() / 1.7f).top();
    layout.row().center();

    for (final Transition outgoing : node.outgoing) {
      String label = outgoing.getLabel();
      ScriptResult enabled = executeScript((String) outgoing.getTransitionProperty("enabled"));
      boolean addOption = true;

      if (enabled.isExecuted() && (Boolean) enabled.getValue() == false) {
        addOption = false;
      }

      if (addOption) {
        final TextButton abutton = new TextButton(label == null ? "Continue" : label, skin);
        layout.add(abutton).padTop(10).size(Gdx.graphics.getWidth(), 60);
        layout.row();

        abutton.addListener(new ChangeListener() {
          public void changed (ChangeEvent event, Actor actor) {
            Jam.this.executeScript((String) outgoing.getTransitionProperty("onTake"));
            enterState(graph.getStates().get(outgoing.getTarget()), graph, layout);
          }
        });
      }
    }

    if (node.outgoing.isEmpty()) {
      TextButton menuButton = new TextButton("Back to Menu", skin);
      layout.add(menuButton).padTop(10).size(Gdx.graphics.getWidth(), 60);
      menuButton.addListener(new ChangeListener() {
        @Override
        public void changed(ChangeEvent event, Actor actor) {
          initMenu(stage);
        }
      });
    }

  }

  public boolean isSet(String key, Object compareValue) {
    if (this.variables.get(key) != null && compareValue != null) {
      return compareValue.equals(this.variables.get(key));
    }
    return false;
  }

  public void setVar(String key, Object value) {
    this.variables.put(key, value);
  }

  private ScriptResult executeScript(String script) {
    if (script != null && !script.trim().isEmpty()) {
      return new ScriptResult(true, ScriptApi.runScript(this, script));
    }
    return new ScriptResult(false, null);
  }

  @Override
  public void render () {

    if(tweenmanager != null)
      tweenmanager.update(Gdx.graphics.getRawDeltaTime());

    Gdx.gl.glClearColor(rf, gf, bf, 1);
    Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

    if (tiledMapHandler != null) {
      tiledMapHandler.getCamera().update();
      tiledMapHandler.render();
    }

    stage.act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
    stage.draw();

    Table.drawDebug(stage);
  }

  @Override
  public void resize (int width, int height) {
    stage.setViewport(width, height, false);
  }

  @Override
  public void dispose () {
    stage.dispose();
    skin.dispose();
  }

}
