package monkeybros.connectjam.core;

/**
 * Created with IntelliJ IDEA.
 * User: drobisch
 * Date: 4/28/13
 * Time: 12:34 PM
 * To change this template use File | Settings | File Templates.
 */

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.tiled.TileSet;
import com.badlogic.gdx.graphics.g2d.tiled.TiledMap;
import com.badlogic.gdx.graphics.g2d.tiled.TiledObject;
import com.badlogic.gdx.graphics.g2d.tiled.SimpleTileAtlas;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.Color;


public class TiledMapObject  {

    private final int width;
    private final int height;
    private float stateTime;

    private final TextureRegion region;
    private float           x,y;
    private float           x_offset,y_offset;
    private SimpleTileAtlas atlas;
    private Color           color;

    TextureRegion[][]       animation_regions;
    Animation[]             animationList;
    Animation               animation = null;



    public TiledMapObject(TiledMap map, TiledObject obj) {
        width   = obj.width;
        height  = obj.height;

        x = obj.x;
        y = Gdx.graphics.getHeight()-obj.y;
        x_offset = 0.0f;
        y_offset = -120.0f;

        color = new Color(1.0f,1.0f,1.0f,1.0f);
        TileSet tileSet = getTileSet(map, obj.gid);
        Texture texture = new Texture(Gdx.files.internal("tilesets/"+tileSet.imageName));
        this.region = new TextureRegion(texture, 0, 0, tileSet.tileWidth, tileSet.tileHeight);
        animation_regions = TextureRegion.split(texture, tileSet.tileWidth, tileSet.tileHeight);
        animationList = new Animation [4];
        animationList[0] = new Animation (0.25f,animation_regions[0][0]);
        animationList[1] = new Animation (0.25f,animation_regions[0][1],animation_regions[1][1],animation_regions[2][1]);
        animationList[1].setPlayMode(Animation.LOOP_PINGPONG);
        animationList[2] = new Animation (0.25f,animation_regions[0][2]);
        animationList[3] = new Animation (0.25f,animation_regions[0][3]);
        animation = animationList[1];
        stateTime=0.0f;
    }

    public TileSet getTileSet(TiledMap map,int gid){
        TileSet foundSet = null;
        for (TileSet set : map.tileSets){
            if (gid >= set.firstgid) {
                foundSet = set;
            }
        }
        return foundSet;
    }

    public void draw(SpriteBatch batch, float parentAlpha) {
        /* Draw */
        if(animation != null)
        {
            stateTime += Gdx.graphics.getDeltaTime();
            batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);
            batch.draw(animation.getKeyFrame(stateTime, true), this.x, this.y+y_offset);
        }
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public void setX(float x) {
        this.x = x;
    }

    public void setY(float y) {
        this.y = y;
    }

}
