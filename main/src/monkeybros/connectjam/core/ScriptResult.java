package monkeybros.connectjam.core;

public class ScriptResult {
  boolean executed = false;
  Object value = null;

  public ScriptResult(boolean executed, Object value) {
    this.executed = executed;
    this.value = value;
  }

  public boolean isExecuted() {
    return executed;
  }

  public void setExecuted(boolean executed) {
    this.executed = executed;
  }

  public Object getValue() {
    return value;
  }

  public void setValue(Object value) {
    this.value = value;
  }
}
