package monkeybros.connectjam.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StateGraph {

  Map<String, State> states = new HashMap<String, State>();
  List<Transition> transitions = new ArrayList<Transition>();

  Map<String, String> stateAttributes = new HashMap<String, String>();
  Map<String, String> transitionAttributes = new HashMap<String, String>();

  public StateGraph() {
  }

  public Map<String, State> getStates() {
    return states;
  }

  public List<Transition> getTransitions() {
    return transitions;
  }

  public State getStartNode() {
    for (Map.Entry<String, State> nodeEntry : states.entrySet()) {
      if (nodeEntry.getValue().incoming.size() == 0) {
        return nodeEntry.getValue();
      }
    }
    throw new RuntimeException("No start node defined");
  }

  public State addState(State node) {
    states.put(node.getId(), node);
    return node;
  }

  public Transition addTransition(Transition edge) {
    transitions.add(edge);
    states.get(edge.getSource()).outgoing.add(edge);
    states.get(edge.getTarget()).incoming.add(edge);
    return edge;
  }

  public Map<String, String> getStateAttributes() {
    return stateAttributes;
  }

  public void addStateAttribute(String attributeName, String keyId) {
    stateAttributes.put(attributeName, keyId);
  }

  public Map<String, String> getTransitionAttributes() {
    return transitionAttributes;
  }

  public void addTransitionAttribute(String attributeName, String keyId) {
    transitionAttributes.put(attributeName, keyId);
  }

  @Override
  public String toString() {
    return "StateGraph{" +
      "states=" + states +
      ", transitions=" + transitions +
      ", stateAttributes=" + stateAttributes +
      ", transitionAttributes=" + transitionAttributes +
      '}';
  }
}
