package monkeybros.connectjam.core.parser;

import org.xmlpull.v1.XmlPullParser;

import java.util.HashMap;
import java.util.Map;

public class ElemScope {
  public Object valueObj;
  public String tag;
  public Map<String, String> attributes = new HashMap<String, String>();

  public ElemScope(String tag) {
    this.tag = tag;
  }

  public void handleText(XmlPullParser parser) {
    String text = parser.getText();
    if (text != null && !text.trim().isEmpty()){
      attributes.put("text", parser.getText());
    }
  }
}
