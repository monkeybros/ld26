package monkeybros.connectjam.core.parser;

import org.xmlpull.v1.XmlPullParser;

import java.util.Stack;

public class ElemHandler {

  public void handleStart(XmlPullParser parser,  Stack<ElemScope> scope) {
    ElemScope newScope = new ElemScope(parser.getName());
    scope.push(newScope);
    start(newScope, parser, scope);
  }

  public void start(ElemScope newScope, XmlPullParser parser, Stack<ElemScope> scope) {
  }

  public void handleEnd(XmlPullParser parser,  Stack<ElemScope> scope) {
    ElemScope oldScope = scope.pop();
    end(oldScope, parser, scope);
  }

  public void end(ElemScope oldScope, XmlPullParser parser, Stack<ElemScope> scope) {
  }
}
