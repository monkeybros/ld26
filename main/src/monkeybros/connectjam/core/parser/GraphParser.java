package monkeybros.connectjam.core.parser;

import com.badlogic.gdx.Gdx;
import monkeybros.connectjam.core.GraphElem;
import monkeybros.connectjam.core.State;
import monkeybros.connectjam.core.StateGraph;
import monkeybros.connectjam.core.Transition;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class GraphParser {

  private String path;
  private InputStream input;

  public GraphParser() {
  }

  public GraphParser(String path) {
    this.path = path;
  }

  public StateGraph parse() {
    final StateGraph graph = new StateGraph();
    Stack<ElemScope> scope = new Stack<ElemScope>();

    Map<String, ElemHandler> handlers = new HashMap<String, ElemHandler>();

    handlers.put("node", new ElemHandler() {
      @Override
      public void start(ElemScope newScope, XmlPullParser parser, Stack<ElemScope> scope) {
        newScope.valueObj = graph.addState(new State(graph, parser.getAttributeValue(null, "id")));
      }
    });

    handlers.put("edge", new ElemHandler() {
      @Override
      public void start(ElemScope newScope, XmlPullParser parser, Stack<ElemScope> scope) {
        newScope.valueObj = graph.addTransition(new Transition(graph, parser.getAttributeValue(null, "id"), parser.getAttributeValue(null, "source"), parser.getAttributeValue(null, "target")));
      }
    });

    handlers.put("EdgeLabel", new ElemHandler() {
      @Override
      public void end(ElemScope oldScope, XmlPullParser parser, Stack<ElemScope> scope) {
        ((GraphElem) scope.firstElement().valueObj).setLabel(oldScope.attributes.get("text"));
      }
    });

    handlers.put("data", new ElemHandler() {
      @Override
      public void start(ElemScope newScope, XmlPullParser parser, Stack<ElemScope> scope) {
        scope.peek().attributes.put("key", parser.getAttributeValue(null, "key"));
      }

      @Override
      public void end(ElemScope oldScope, XmlPullParser parser, Stack<ElemScope> scope) {
        if (!scope.isEmpty()) { // graph also can have data tags
          if (scope.peek().valueObj instanceof GraphElem) {
            ((GraphElem) scope.peek().valueObj).setProperty(oldScope.attributes.get("key"), oldScope.attributes.get("text"));
          }
        }
      }
    });

    handlers.put("key", new ElemHandler() {
      @Override
      public void start(ElemScope newScope, XmlPullParser parser, Stack<ElemScope> scope) {
        String forType = parser.getAttributeValue(null, "for");
        if (forType == null) {
          return;
        }else if (forType.equals("node")) {
          graph.addStateAttribute(parser.getAttributeValue(null, "attr.name"), parser.getAttributeValue(null, "id"));
        }
        else if (forType.equals("edge")) {
          graph.addTransitionAttribute(parser.getAttributeValue(null, "attr.name"), parser.getAttributeValue(null, "id"));
        }
      }
    });

    try {
      XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
      factory.setNamespaceAware(true);
      XmlPullParser xpp = factory.newPullParser();

      xpp.setInput(readFile(path), "UTF-8");

      int eventType = xpp.getEventType();
      while (eventType != XmlPullParser.END_DOCUMENT) {
        if(eventType == XmlPullParser.START_DOCUMENT) {
        } else if(eventType == XmlPullParser.START_TAG) {
          ElemHandler handler = handlers.get(xpp.getName());
          if (handler != null) {
            handler.handleStart(xpp, scope);
          }
        } else if(eventType == XmlPullParser.END_TAG) {
          ElemHandler handler = handlers.get(xpp.getName());
          if (handler != null) {
            handler.handleEnd(xpp, scope);
          }
        } else if(eventType == XmlPullParser.TEXT) {
          if (!scope.isEmpty()) {
            scope.peek().handleText(xpp);
          }
        }
        eventType = xpp.next();
      }
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
    return graph;
  }

  private InputStream readFile(String path) {
    if (this.input == null) {
      return Gdx.files.internal(path).read();
    }
    return this.input;
  }

  public void setInput(InputStream input) {
    this.input = input;
  }
}
