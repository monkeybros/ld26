package monkeybros.connectjam.core;

public class GameSettings {
  public boolean isTtsEnabled() {
    return ttsEnabled;
  }

  public void setTtsEnabled(boolean ttsEnabled) {
    this.ttsEnabled = ttsEnabled;
  }

  boolean ttsEnabled = false;
}
