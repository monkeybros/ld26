package monkeybros.connectjam.core;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.tiled.*;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;

import java.util.ArrayList;

public class TiledMapHandler {
    private static final int[] layersList = { 0,1 };
    private SpriteBatch batch = null;

    public void render() {
        tileMapRenderer.getProjectionMatrix().set(camera.combined);

        Vector3 tmp = new Vector3();
        tmp.set(0, 0, 0);
        camera.unproject(tmp);

        tileMapRenderer.render((int) tmp.x, (int) tmp.y,
                Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), layersList);

        if(batch != null) {
            batch.begin();
                for (TiledMapObject obj : objectList) {
                    obj.draw(batch,1.0f);
                }
            batch.end();
        }

    }

    public OrthographicCamera getCamera() {
        if (camera == null) {
            throw new IllegalStateException(
                    "getCamera() caused execption!");
        }
        return camera;
    }

    public void initCamera(int scrnWidth, int scrnHeight) {
        camera = new OrthographicCamera(scrnWidth, scrnHeight);
        camera.position.set(0, 0, 0);
    }

    public void setPackageDirectory(String packageDirectory) {
        PackageFileDirectory = Gdx.files.internal(packageDirectory);
    }

    public void dispose() {
        tileAtlas.dispose();
        tileMapRenderer.dispose();
    }

    public TiledMap getMap() {
        return map;
    }

    public int getHeight() {
        return map.height * map.tileHeight;
    }

    public int getWidth() {
        return map.width * map.tileWidth;
    }

    public void loadMap(String tmxFilePath) {
        if (PackageFileDirectory == null) {
            throw new IllegalStateException("loadMap() caused execption!. 'PackerFileDirectory' is null.");
        }
        batch = new SpriteBatch();
        map = TiledLoader.createMap(Gdx.files.internal(tmxFilePath));
        tileAtlas = new SimpleTileAtlas(map, PackageFileDirectory);
        tileMapRenderer = new TileMapRenderer(map, tileAtlas, map.tileWidth, map.tileHeight);

        objectList = new ArrayList<TiledMapObject>();
        objectList.clear();
        for(int i = 0;i< map.objectGroups.get(0).objects.size();i++)
        {
            objectList.add(new TiledMapObject(map, map.objectGroups.get(0).objects.get(i)));
        }
    }

    public ArrayList<TiledMapObject> getObjectList() {
        return objectList;
    }

    private OrthographicCamera camera;
    private FileHandle PackageFileDirectory;
    private TiledMap map;
    private TileAtlas tileAtlas;
    private TileMapRenderer tileMapRenderer;
    public ArrayList<TiledMapObject> objectList;
}
