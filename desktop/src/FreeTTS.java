import com.sun.speech.freetts.Voice;
import com.sun.speech.freetts.VoiceManager;
import monkeybros.connectjam.core.TextToSpeech;

public class FreeTTS implements TextToSpeech {
  VoiceManager voiceManager = VoiceManager.getInstance();
  private Thread currentTalk;

  @Override
  public void talk(final String text) {
    if (currentTalk != null) {
      currentTalk.interrupt();
    }

    Runnable talkRunnable = new Runnable() {
      @Override
      public void run() {
        Voice voice = voiceManager.getVoice("kevin16");
        voice.allocate();
        voice.speak(text);
        voice.deallocate();
      }
    };
    currentTalk = new Thread(talkRunnable);
    currentTalk.start();
  }
}
