import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import monkeybros.connectjam.core.GameSettings;
import monkeybros.connectjam.core.Jam;

public class DesktopStarter {
  public static void main(String[] args) {
    LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
    cfg.title = "Adventure";
    cfg.useGL20 = true;
    cfg.width = 800;
    cfg.height = 600;
    cfg.fullscreen = false;
    new LwjglApplication(new Jam(new GameSettings(), new FreeTTS()), cfg);
  }
}
