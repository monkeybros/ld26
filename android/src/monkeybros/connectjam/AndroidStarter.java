package monkeybros.connectjam;

import android.os.Bundle;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import monkeybros.connectjam.core.GameSettings;
import monkeybros.connectjam.core.Jam;
import monkeybros.connectjam.core.TextToSpeech;

public class AndroidStarter extends AndroidApplication {

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    AndroidApplicationConfiguration cfg = new AndroidApplicationConfiguration();
    cfg.useAccelerometer = false;
    cfg.useCompass = false;
    cfg.useWakelock = true;
    cfg.useGL20 = true;
    initialize(new Jam(new GameSettings(), new TextToSpeech() {
      @Override
      public void talk(String text) {
      }
    }), cfg);
  }

}