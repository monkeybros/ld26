# Overview

This is our entry for the Ludum Dare 26. Its a text adventure engine with modding capabilities and a example story. A sample story called _Captain Leslie: Adventures in Space_ is included.

You can use [yEd](http://www.yworks.com/de/products_yed_about.html) to edit the story graph (*.graphml files).

# How to edit or create a new story

In the distribution jar is a file called _stories_, which is the list of stories available in the dropdown after starting the
game. You can edit / update this file with your favorite zip tool.
When you add a new story, you also need to put the corresponding graphml file to the root of the jar file.

# Script API

There are two event properties which may contain JavaScript code:

_onEnter_ for states
_onTake_ for transitions

Aditionally, the transition has a _enabled_ property where JavaScript is expected to return a boolean value.

The API is bound to the underscore '\_'.

See leslie.graphml to get an idea.

##Methods and Properties
    _.game : the instance of the game
    _.game.isSet(varname, value) : check if a variable has a given value
    _.game.setVar(varname, value) : set a varibale
    _.playSound(pathToResource) : play sound from classpath
    _.playMusic(pathToResource) : play music from classpath

# Credits

libgdx and RhinoJS was used.

Code: Marcus Drobisch, Andreas Drobisch

Story: Thimo Hörster

##Tiles

Thomas Bruno : [http://opengameart.org/content/1950s-scifi-setting-indoor-tileset-with-kitchen-appliances]

Trent Gamblin : [http://opengameart.org/content/48-animated-old-school-rpg-characters-16x16]

## Music

Arjen Schumacher (Hardmoon) : http://www.enntess.org/index.php?page=releases&id=5



